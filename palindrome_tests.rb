require 'rspec/autorun'
require_relative 'palindrome'

RSpec.describe 'Palindrome tests' do
  context 'true' do
    it 'returns true for palindrome strings' do
      strings = %w[1 123321 radar abba]
      strings.each do |str|
        result = Palindrome.new.palindrome?(str)
        expect(result).to be true
      end
    end
  end

  context 'false' do
    it 'returns false for normal strings' do
      strings = %w[12 123 none]
      strings.each do |str|
        result = Palindrome.new.palindrome?(str)
        expect(result).to be false
      end
    end

    it 'returns false for empty string' do
      string = ''
      result = Palindrome.new.palindrome?(string)
      expect(result).to be false
    end
  end
end
