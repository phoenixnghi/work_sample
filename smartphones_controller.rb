class Api::V1::SmartphonesController < Api::V1::BaseController
  before_action :authorize_admin

  def create
    smartphone = Smartphone.create!(create_params)

    render json: success_response(smartphone)
  end

  private

  def create_params
    smartphones_keys         = :data_memory, :year_of_manufacture, :color, :price, :os_version
    nested_manufacturer_keys = [:brand_name, :model, :type]
    params.permit(smartphones_keys,
                  manufacturer_attributes: nested_manufacturer_keys)
  end

  # app/controllers/concerns/response_helper.rb
  def success_response(data)
    {
      status: 'success',
      code:   200,
      data:   data
    }
  end
end
