class Palindrome
  def palindrome?(str)
    str = str.to_s
    return false if str.size.zero?

    left  = 0
    right = str.size - 1

    right.times do
      break if left == right
      return false if str[left] != str[right]

      left  += 1
      right -= 1
    end
    true
  end
end
