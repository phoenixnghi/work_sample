class Matrix
  # Ruby has lots of magic methods
  # def magic_anti_clockwise_rotate(matrix)
  #   return [] if matrix.flatten.empty?
  #
  #   matrix.transpose.reverse
  # end

  # Although using Ruby's magic methods is fun,
  # but I always want to understand the logic behind.
  def anti_clockwise_rotate(matrix)
    return [] if matrix.flatten.empty?

    result = []

    matrix.size.times do |i|
      new_row = []
      matrix.each do |row|
        new_row << row[i]
      end
      result << new_row
    end
    result.reverse
  end
end
