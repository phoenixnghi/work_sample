require 'rspec/autorun'
require_relative 'matrix'

RSpec.describe 'Matrix anti clockwise rotation' do
  context 'valid' do
    it 'returns the same for 1x1 matrix' do
      matrix_1x1 = [[1]]
      expected   = [[1]]

      result = Matrix.new.anti_clockwise_rotate(matrix_1x1)
      expect(result).to eq(expected)
    end

    it 'rotates the 2x2 matrix anti clockwise' do
      matrix_2x2 = [[1, 2], [3, 4]]
      expected   = [[2, 4], [1, 3]]

      result = Matrix.new.anti_clockwise_rotate(matrix_2x2)
      expect(result).to eq(expected)
    end

    it 'rotates the 4x4 matrix anti clockwise' do
      matrix_4x4 = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
      expected   = [[4, 8, 12, 16], [3, 7, 11, 15], [2, 6, 10, 14], [1, 5, 9, 13]]

      result = Matrix.new.anti_clockwise_rotate(matrix_4x4)
      expect(result).to eq(expected)
    end

    it 'rotates the 8x8 matrix anti clockwise' do
      matrix_8x8 = (1..64).each_slice(8).to_a
      expected   = [[8, 16, 24, 32, 40, 48, 56, 64], [7, 15, 23, 31, 39, 47, 55, 63], [6, 14, 22, 30, 38, 46, 54, 62], [5, 13, 21, 29, 37, 45, 53, 61], [4, 12, 20, 28, 36, 44, 52, 60], [3, 11, 19, 27, 35, 43, 51, 59], [2, 10, 18, 26, 34, 42, 50, 58], [1, 9, 17, 25, 33, 41, 49, 57]]

      result = Matrix.new.anti_clockwise_rotate(matrix_8x8)
      expect(result).to eq(expected)
    end
  end

  context 'invalid' do
    it 'returns empty array for 0x0 matrix' do
      matrix_0x0 = [[]]
      expected   = []

      result = Matrix.new.anti_clockwise_rotate(matrix_0x0)
      expect(result).to eq(expected)
    end
  end
end
